import React from 'react';

import iumberella from '../images/icons/icon-umberella.png';
import iwind from '../images/icons/icon-wind.png';
import icompass from '../images/icons/icon-compass.png';

const PUBLIC_URL = process.env.PUBLIC_URL;

export default function CityCondition(props) {
  const { city, weather, temp, icon, humidity, wind, winddir } = props.data;
  if (!city) return null;
  const unit = props.unit;
  console.log('render condition');
  return (
    <div>
      <div className="weather-condition__location">{city}</div>
      <div style={{ textAlign: 'center', fontSize: 14 }}>{weather}</div>
      <div className="weather-condition__temp">
        <span style={{ paddingRight: 18 }}>{`${temp[unit]} ${unit.toLowerCase()}`}</span>
        {/* <img style={{ width: 88, height: 60 }} src={`${PUBLIC_URL}/icons/${icon}.svg`} /> */}
      </div>
      <div className="weather-condition__desc">
        <div>
          <img src={iumberella} /> <span className="citem">{humidity}</span>
        </div>
        <div>
          <img src={iwind} /> <span className="citem">{`${wind} km/h`}</span>
        </div>
        <div>
          <img src={icompass} /> <span className="citem">{winddir}</span>
        </div>
      </div>
    </div>
  );
}
