import React, { Component } from 'react';

class AutoRefreshSwitch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      reloading: false,
      switchOn: false,
      countDown: props.interval,
      // counting
    };
  }

  reload() {
    this.setState({ reloading: true });
    clearInterval(this._reloadTimer);
    clearInterval(this._countTimer);
    this.props.onCitySubmit();
    // .then(() => {
    //   this.setState({countDown: 60});
    //   this._countTimer = setInterval(() => {
    //     this.setState({ countDown: this.state.countDown - 1 });
    //   }, 1000);

    // })
  }

  onSwitch() {
    this.setState(prevState => {
      return {
        switchOn: !prevState.switchOn,
      };
    });
    this._reloadTimer = setInterval(this.props.onTimerEnd, this.props.interval * 1000);
    this._countTimer = setInterval(() => {
      this.setState({ countDown: this.state.countDown - 1 });
    }, 1000);
  }

  render() {
    const { switchOn } = this.state;
    return (
      <div style={{ display: 'flex', alignItems: 'center' }}>
        <span style={{ marginRight: 15 }}>{`${this.state.countDown} s`}</span>
        <div className="onoffswitch">
          <input
            checked={switchOn}
            type="checkbox"
            name="onoffswitch"
            className="onoffswitch-checkbox"
            id="myonoffswitch"
            onChange={() => this.onSwitch()}
          />
          <label className="onoffswitch-label" htmlFor="myonoffswitch">
            <span className="onoffswitch-inner" />
            <span className="onoffswitch-switch" />
          </label>
        </div>
      </div>
    );
  }
}
AutoRefreshSwitch.defaultProps = { interval: 10 };

export default function Toolbar({ curCity, unit, onCityChange, onCitySubmit, onUnitChange }) {
  return (
    <nav>
      <div style={{ flex: 1 }}>
        <input className="search-input" value={curCity} onChange={onCityChange} />
        <button className="search-btn" onClick={onCitySubmit}>
          <i className="fa fa-search" />
        </button>
        <button
          className="temp-switch"
          onClick={() => {
            unit == 'C' ? onUnitChange('F') : onUnitChange('C');
          }}>
          <i className="fa fa-thermometer-empty" style={{ marginRight: 8 }} />
          {unit.toUpperCase()}
        </button>
      </div>

      <AutoRefreshSwitch onTimerEnd={onCitySubmit} />
    </nav>
  );
}
