import React from 'react';

import CityConditon from './CityConditon';
import Forecast from './Forecast';

import {fetchConditionData, fetchForecast} from './api/weather';

class WeatherChannel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      curCity: 'brisbane',
      condition: {},
      days: [],
    };
  }

  handleCityChange(event) {
    const value = event.target.value;
    this.setState({curCity: value});
  }

  onConditionLoad(data) {
    const condition = {
      city: data.display_location.full,
      temp: {C:data.temp_c, F:data.temp_f},
      weather: data.weather
    }
    this.setState({condition});
  }
  handleSearch() {
    const city = this.state.curCity;
    fetchConditionData(city).then(data => {
      this.onConditionLoad(data);
    });
    fetchForecast(city).then(data => {
      this.onForecastLoad()
    })
  }
  
  render() {
    return (
      <React.Fragment>
      <nav>
      <div style={{flex:1}}>
        <input className="search-input" value={this.state.curCity} onChange={this.handleCityChange.bind(this)} />
        <button className="search-btn" onClick={this.handleSearch.bind(this)}><i class="fa fa-search"></i></button>
        </div>
        </nav>
      <main>
        <section className="weather-conditio">
          <CityConditon data={this.state.condition} />
        </section>
        <section className="weather-forecast">
          <Forecast days={this.state.days} />
        </section>
      </main>
      </React.Fragment>
    );
  }
}

function Forecast(props) {
  const days = props.days;
  let rows = [];
  // for (let i = 0; i < days.length; i++) {
  //   let day = days[i];
  //   rows.push(row);
  // }
  rows = days.map(day => {
    return (
      <div key={} className="weather-forecast__row">
        <span className="weather-forecast__day">{day.weekday}</span>
        <span class="weather-forecast__icon">
          <img src={day.icon} />
        </span>
        <span class="weather-forecast__high">{day.high}</span>
        <span class="weather-forecast__low">{day.low}</span>
      </div>
    );
  });
  return <div>{rows}</div>;

  days.map((day, index) => {return <Row key={`${day.weekday}_${index}`} day={day} />})
}
function DailyItem(props) {
  const day = props.day;
  return (
    <div class="weather-forecast__row">
      <span class="weather-forecast__day">{day.weekday}</span>
      <span class="weather-forecast__icon">
        <img src={day.icon} />
      </span>
      <span class="weather-forecast__high">{day.high}</span>
      <span class="weather-forecast__low">{day.low}</span>
    </div>
  );
}


// weather.js
import axios from 'axios';


const CONDITION_BASE_URL =
  'http://api.wunderground.com/api/f029e46fd0232d12/geolookup/conditions/q/Australia/';
const FORECAST_BASE_URL =
  'http://api.wunderground.com/api/f029e46fd0232d12/geolookup/forecast10day/q/Australia/';

export function fetchConditionData(city) {
  const url = `${CONDITION_BASE_URL}${city}.json`;
  return axios.get(url).then(response => response.data.current_observation);
}
// fetch = () => axios.get(url)