import React from 'react';

const PUBLIC_URL = process.env.PUBLIC_URL;
const mapping = {
  partlycloudy: 'partlycloudy.svg',
  mostlycloudy: 'mostlycloudy.svg',
  cloudy: 'cloudy.svg',
  clear: 'clear.svg',
};

function DailyItem({ day, unit }) {
  console.log(`render day ${day.weekday}`);
  return (
    <div className="weather-forecast__row">
      <span className="weather-forecast__day">{day.weekday}</span>
      <span className="weather-forecast__icon">
        <img src={`${PUBLIC_URL}/icons/${day.icon}.svg`} />
      </span>
      <span className="weather-forecast__high">{day.high[unit]}</span>
      <span className="weather-forecast__low">{day.low[unit]}</span>
    </div>
  );
}

function Forecast__({ days, unit, numOfDays }) {
  return days
    .slice(0, numOfDays)
    .map((day, i) => <DailyItem key={day.key} day={day} unit={unit} />);
}

export default class Forecast extends React.Component {
  constructor(props) {
    super(props);
    this.state = { numOfDays: 5 };
  }
  render() {
    const { days, unit } = this.props;
    const { numOfDays } = this.state;
    console.log('render forecast');
    let btnClass0 =
      numOfDays == 5 ? ['forecast__switch_0 switch-active'].join(' ') : 'forecast__switch_0';
    let btnClass1 =
      numOfDays == 10 ? ['forecast__switch_1 switch-active'].join(' ') : 'forecast__switch_1';
    return (
      <div>
        <div className="forecast__switch">
          <button className={btnClass0} onClick={e => this.setState({ numOfDays: 5 })}>
            5 days
          </button>
          <button className={btnClass1} onClick={e => this.setState({ numOfDays: 10 })}>
            10 days
          </button>
        </div>
        {days.slice(0, numOfDays).map((day, i) => (
          <DailyItem key={day.key} day={day} unit={unit} />
        ))}
      </div>
    );
  }

  // render() {
  //   const n = this.state.numOfDays;
  //   const newDays = this.props.days.slice(0, n);
  //   return newDays.map(day => {
  //     return <DailyItem day={day} />;
  //   });
  // }
}
