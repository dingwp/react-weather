import React from 'react';

export default function Header() {
  return (
    <footer className="weather-channel__footer">
      <p> Powered by React</p>
    </footer>
  );
}
